﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rideSharing.Services.classes
{
    public class RideRespons
    {
        public Ride Ride { get; set; }
        public string Type { get; set; }

        public RideRespons(Ride _ride, string _type)
        {
            Ride =_ride;
            Type = _type;
        }
    }
}
