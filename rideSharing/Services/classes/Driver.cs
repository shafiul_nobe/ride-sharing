﻿using System;

namespace rideSharing.Services
{
    public class Driver : IDriver
    {
        public string Id { get ; set ; }
        public string PhoneNumber { get ; set ; }
        public bool InRide { get ; set ; }
        public string Status { get ; set ; }
        public string RiderId { get ; set ; }

        public Driver(string _phoneNumber)
        {
            PhoneNumber = _phoneNumber;
            Id = Guid.NewGuid().ToString();
            Status = "offline";
            InRide = false;
        }

        public void RideStart(string _riderId)
        {
            RiderId = _riderId;
            InRide = true;
        }

        public void RideEnd()
        {
            RiderId = null;
            InRide = false;
        }

        public void SetStatus(string _status)
        {
           Status = _status;
        }
    }
}
