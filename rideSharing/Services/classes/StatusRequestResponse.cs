﻿using rideSharing.Services.classes;

namespace rideSharing.Services
{
    public class StatusRequestResponse : IStatusRequestResponse
    {
        public string DriverId { get ; set ; }
        public string RiderId { get ; set ; }
        public string Status { get ; set ; }
        public string Id { get ; set ; }
        public string Phone { get ; set ; }
        public string type { get; set; }

        public StatusRequestResponse(Ride _ride, string _type)
        {
            DriverId = _ride.DriverId;
            RiderId = _ride.RiderId;
            Status = _ride.Status;
            Id = _ride.Id;
            type = _type;
        }
    }
}
