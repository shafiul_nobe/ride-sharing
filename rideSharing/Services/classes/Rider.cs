﻿using System;

namespace rideSharing.Services
{
    public class Rider : IRider
    {
        public string Id { get ; set ; }
        public string PhoneNumber { get ; set ; }
        public bool InRide { get ; set ; }
        public string DriverId { get ; set ; }

        public Rider(string _phoneNumber)
        {
            PhoneNumber = _phoneNumber;
            Id = Guid.NewGuid().ToString();
            InRide = false;
        }
        public void RideStart(string _driverId)
        {
            InRide = true;
            DriverId = _driverId;
        }

        public void RideEnd()
        {
            InRide = false;
            DriverId = null;
        }
    }
}
