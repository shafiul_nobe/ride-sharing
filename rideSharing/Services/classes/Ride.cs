﻿using rideSharing.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rideSharing.Services.classes
{
    public class Ride : IRide
    {
        public string Id { get ; set ; }
        public string DriverId { get ; set ; }
        public string RiderId { get ; set ; }
        public string Status { get ; set ; }

        public Ride(string _driverId , string _riderId)
        {
            Id = Guid.NewGuid().ToString();
            DriverId = _driverId;
            RiderId = _riderId;
            Status = "start";
        }
        public Ride()
        {

        }

        public void End()
        {
            Status = "end";
        }

    }
}
