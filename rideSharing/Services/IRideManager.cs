﻿using rideSharing.Services.classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rideSharing.Services
{
    public interface IRideManagerService
    {
        public List<Driver> GetAllDriver();
        public List<Rider> GetAllRiders();
        public Driver RegisterDriver(string _phoneNumber);
        public Rider RegisterRider(string _phoneNumber);
        public Driver SetDriverStatus(string _id, string _status);
        public RideRespons requestForRideStart(string id);
        public RideRespons requestForRideEnd(string id);
        public StatusRequestResponse GetStatus(string _driverId, string _riderId);
    }
}
