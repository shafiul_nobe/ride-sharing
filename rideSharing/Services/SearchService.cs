﻿using rideSharing.Services.classes;
using System.Collections.Generic;

namespace rideSharing.Services
{
    public class SearchService
    {
        
        
        public static Rider GetRiderById(string _riderId, List<Rider> _riders)
        {
            for (int i = 0; i < _riders.Count; i++)
            {
                if (_riders[i].Id == _riderId)
                    return _riders[i];
            }
            return null;
        }

        public static Rider GetRiderByPhone(string _phone, List<Rider> _riders)
        {
            for (int i = 0; i < _riders.Count; i++)
            {
                if (_riders[i].PhoneNumber == _phone)
                    return _riders[i];
            }
            return null;
        }

        public static Driver GetDriverByPhone(string _phone, List<Driver> _drivers)
        {
            for (int i = 0; i < _drivers.Count; i++)
            {
                if (_drivers[i].PhoneNumber == _phone)
                    return _drivers[i];
            }
            return null;
        }

        public static string GetRiderPhoneById(string _riderId, List<Rider> _riders)
        {
            for (int i = 0; i < _riders.Count; i++)
            {
                if (_riders[i].Id == _riderId)
                    return _riders[i].PhoneNumber;
            }
            return null;
        }
        public static Ride GetRideByDriverId(string _driverId, List<Ride> _rides)
        {
            for (int i= 0; i < _rides.Count; i++)
            {
                if (_rides[i].DriverId == _driverId && _rides[i].Status=="start")
                    return _rides[i];
            }
            return null;
        }

        public static Ride GetRideByRiderOrDriverId(string _driverId, string _riderId, List<Ride> _rides)
        {
            for (int i = 0; i < _rides.Count; i++)
            {
                if ((_rides[i].RiderId == _riderId || _rides[i].DriverId == _driverId) && _rides[i].Status=="start")
                    return _rides[i];
            }
            return null;
        }

        public static Driver GetDriverById(string DriverId, List<Driver> _drivers)
        {
            for (int i = 0; i < _drivers.Count; i++)
            {
                if (_drivers[i].Id == DriverId)
                    return _drivers[i];
            }
            return null;
        }

        public static Driver FindAvailableOnlineDriver(Rider Rider, List<Driver> _drivers)
        {
            for (int i = 0; i < _drivers.Count; i++)
            {
                if (_drivers[i].Status == "online" && _drivers[i].InRide == false && _drivers[i].PhoneNumber!= Rider.PhoneNumber)
                {
                    return _drivers[i];
                }
            }
            return null;
        }
    }
}
