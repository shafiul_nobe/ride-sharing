﻿namespace rideSharing.Services
{
    public interface IDriver
    {
        public string Id { get; set; }
        public string PhoneNumber { get; set; }
        public bool InRide { get; set; }
        public string Status { get; set; }
        public string RiderId { get; set; }
        public void RideStart(string _riderId);
        public void RideEnd();

        public void SetStatus(string _status);
    }
}
