﻿namespace rideSharing.Services
{
    public interface IRider
    {
        public string Id { get; set; }
        public string PhoneNumber { get; set; }
        public bool InRide { get; set; }
        public string DriverId { get; set; }
        public void RideStart(string _driverId);
        public void RideEnd();
    }
}
