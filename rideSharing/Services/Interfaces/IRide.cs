﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rideSharing.Services.Interfaces
{
    interface IRide
    {
        public string Id { get; set; }
        public string DriverId { get; set; }
        public string RiderId { get; set; }
        public string Status  { get; set; }

    }
}
