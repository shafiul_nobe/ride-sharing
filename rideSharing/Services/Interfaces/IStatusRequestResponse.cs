﻿namespace rideSharing.Services
{
    public interface IStatusRequestResponse
    {
        public string DriverId { get; set; }
        public string RiderId { get; set; }
        public string Status { get; set; }
        public string Id { get; set; }
        public string Phone { get; set; }
    }
}
