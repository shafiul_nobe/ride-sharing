﻿using rideSharing.Services.classes;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rideSharing.Services
{

    public class RideManagerService : IRideManagerService
    {
        private readonly List<Driver> _drivers;
        private readonly List<Rider> _riders;
        private readonly List<Ride> _rides;

        public RideManagerService()
        {
            _drivers = new List<Driver>();
            _riders = new List<Rider>();
            _rides = new List<Ride>();
        }

        public List<Driver> GetAllDriver()
        {
            return _drivers;
        }

        public List<Rider> GetAllRiders()
        {
            return _riders;
        }

        public Driver RegisterDriver(string phoneNumber)
        {
            if(SearchService.GetDriverByPhone(phoneNumber,_drivers) != null)
            {
                return null;
            }
            var newDriver = new Driver(phoneNumber);
            _drivers.Add(newDriver);
            return newDriver;
        }

        public Rider RegisterRider(string phoneNumber)
        {
            if (SearchService.GetRiderByPhone(phoneNumber, _riders) != null)
            {
                return null;
            }
            var newRider = new Rider(phoneNumber);
            _riders.Add(newRider);
            return newRider;
        }

        public Driver SetDriverStatus(string id , string status)
        {
            Driver newDriver = null;
            foreach(Driver driver in _drivers)
            {
                if(driver.Id == id && driver.InRide==false)
                {
                    driver.SetStatus(status);
                    newDriver = driver;
                }
            }
            return newDriver;
        }
        public Ride Start(string _driverId, string _riderId)
        {
            for (int i = 0; i < _drivers.Count; i++)
            {
                if(_drivers[i].Id == _driverId)
                {
                    _drivers[i].RideStart(_riderId);
                    break;
                }
            }
            for (int i = 0; i < _riders.Count; i++)
            {
                if (_riders[i].Id == _riderId)
                {
                    _riders[i].RideStart(_driverId);
                    break;
                }
            }
            return new Ride(_driverId, _riderId);
        }

        public Ride End(string _driverId, string _riderId)
        {
            Ride Ride =null;  
            for (int i = 0; i < _drivers.Count; i++)
            {
                if (_drivers[i].Id == _driverId)
                {
                    _drivers[i].RideEnd();
                    break;
                }
            }
            for (int i = 0; i < _riders.Count; i++)
            {   
                if (_riders[i].Id == _riderId)
                {
                    _riders[i].RideEnd();
                    break;
                }
            }
            for (int i = 0; i < _rides.Count; i++)
            {
                if(_rides[i].DriverId ==  _driverId)
                {
                    _rides[i].End();
                    Ride = _rides[i];
                    break;
                }
            }
            return Ride;
        }
        public RideRespons requestForRideEnd(string _driverId)
        {
            Ride Ride = null;
            Driver Driver = SearchService.GetDriverById(_driverId, _drivers);
            if (Driver == null)
                return new RideRespons(Ride, "InvalidDriver");
            if (Driver.InRide == false)
                return new RideRespons(Ride, "NotInRide");
            Ride = SearchService.GetRideByDriverId(_driverId,_rides);
            if (Ride == null)
                return null;
            Ride = End(Ride.DriverId,Ride.RiderId);
             return new RideRespons(Ride, "ok"); ;
        }
        public RideRespons requestForRideStart(string _riderId)
        {
            Ride Ride = null;
            var Rider = SearchService.GetRiderById(_riderId, _riders);
            if (Rider == null )
                return new RideRespons(Ride, "InvalidRider");
            if (Rider.InRide == true)
                return new RideRespons(Ride, "InRide");


            var AvailableDriver = SearchService.FindAvailableOnlineDriver(Rider, _drivers);
            if (AvailableDriver != null)
            {
                Ride =Start(AvailableDriver.Id, Rider.Id);
                _rides.Add(Ride);
                return new RideRespons(Ride,"ok");
            }
            else
                 return new RideRespons(Ride, "NotAvailable"); ;
        }

        public StatusRequestResponse  GetStatus(string _driverId, string _riderId)
        {
            Ride Ride = new Ride();
            if (_driverId != null && SearchService.GetDriverById(_driverId, _drivers) == null)
                return new StatusRequestResponse(Ride, "InvalidDriver");
            if (_riderId != null && SearchService.GetRiderById(_riderId, _riders) == null)
                return new StatusRequestResponse(Ride, "InvalidRider");

            Ride = SearchService.GetRideByRiderOrDriverId(_driverId, _riderId, _rides);
            if (Ride == null)
                return new StatusRequestResponse(new Ride(), "NotFound");
            var Response = new StatusRequestResponse(Ride,"ok");
            Response.Phone = SearchService.GetRiderPhoneById(Response.RiderId,_riders);
            return Response;
        }


    }
}
