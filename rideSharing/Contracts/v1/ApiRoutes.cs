﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rideSharing.Contracts
{
    public static class ApiRoutes
    {
        public const string Root = "api";

        public const string Version = "v1";

        public const string Base = Root + "/" + Version;

        public static class Rides
        {
            
            public const string RegisterRider = Base + "/riders";
            
            public const string RegisterDriver = Base + "/drivers";
            
            public const string GetAllDrivers = Base + "/driverlist";
            
            public const string GetAllRiders = Base + "/riderlist";

            public const string SetDriverStatus = Base + "/drivers/{id}/status";
            
            public const string RideRequest = Base + "/rides";

        }

    }
}
