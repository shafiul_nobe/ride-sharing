﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rideSharing.Contracts.v1.Requests
{
    public class RegistrationRequest
    {
        public string phone { get; set; }
    }
}
