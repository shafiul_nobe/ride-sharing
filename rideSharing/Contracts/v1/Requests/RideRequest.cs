﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rideSharing.Contracts.v1.Requests
{
    public class RideRequest
    {
        public string rider_id { get; set; }
        public string driver_id { get; set; }
    }
}
