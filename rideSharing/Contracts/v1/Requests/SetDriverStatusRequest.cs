﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rideSharing.Contracts.v1.Requests
{
    public class SetDriverStatusRequest
    {
        public string status { get; set; }
    }
}
