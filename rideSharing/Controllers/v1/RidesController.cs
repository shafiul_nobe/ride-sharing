﻿using Microsoft.AspNetCore.Mvc;
using rideSharing.Contracts;
using rideSharing.Contracts.v1.Requests;
using rideSharing.Services;
using rideSharing.Services.classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rideSharing.Controllers
{
    public class RidesController : Controller
    {
        
        private IRideManagerService _rideManagerService;
        public RidesController( IRideManagerService rideManagerService)
        {
            _rideManagerService = rideManagerService; 
        }


        [HttpGet(ApiRoutes.Rides.GetAllDrivers)]
        public IActionResult GetAllDrivers()
        {
            return Ok(_rideManagerService.GetAllDriver());

        }

        [HttpGet(ApiRoutes.Rides.GetAllRiders)]
        public IActionResult GetAllRiders()
        {
            
            return Ok(_rideManagerService.GetAllRiders());

        }


        [HttpPost(ApiRoutes.Rides.RegisterDriver)]
        public IActionResult RegisterDriver([FromBody] RegistrationRequest request)
        {

            try{
                var response = _rideManagerService.RegisterDriver(request.phone);
                return Json(new { phone = response.PhoneNumber, id = response.Id, type = "driver" });
            }
            catch(Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Error = "This phone number has already been registered as driver",
                });
            }
            //return Json(new { phone = response.PhoneNumber, id = response.Id, type = "driver" });
            
        }

        [HttpPost(ApiRoutes.Rides.RegisterRider)]
        public IActionResult RegisterRider([FromBody] RegistrationRequest request)
        {
            

            try
            {
                var response = _rideManagerService.RegisterRider(request.phone);
                return Json(new { phone = response.PhoneNumber, id = response.Id, type = "rider" });
            }catch(Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Error = "This phone number has already been registered as Rider",
                });
            }
        }


        [HttpPut(ApiRoutes.Rides.SetDriverStatus)]
        public IActionResult SetDriverStatus([FromRoute]Guid id, [FromBody] SetDriverStatusRequest request )
        {
            if(request.status != "offline" && request.status != "online")
            {
                return Json(new
                {
                    Success = false,
                    Error = "Invalid status",
                });
            }
            var response = _rideManagerService.SetDriverStatus(id.ToString(),request.status);

            try
            {
                return Json(new { phone = response.PhoneNumber, id = response.Id, type = "driver", status = response.Status });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Error = "Invalid Request",
                });
            }
        }


        [HttpPost(ApiRoutes.Rides.RideRequest)]
        public IActionResult RideRequest([FromBody] RideRequest request)
        {
            RideRespons response = null;
            if(request.rider_id != null)
            {
                response = _rideManagerService.requestForRideStart(request.rider_id);
                if(response.Type== "InvalidRider")
                {
                    return Json(new
                    {
                        Success = false,
                        Error = "Invalid rider_id",
                    });
                }
                if (response.Type == "InRide")
                {
                    return Json(new
                    {
                        Success = false,
                        Error = "You are already in a ride.",
                    });
                }
                try
                {
                    return Json(new { id = response.Ride.RiderId, driver_id = response.Ride.DriverId, rider_id = response.Ride.RiderId, status = response.Ride.Status });
                }
                catch
                {
                    return Json(new
                    {
                        Success = false,
                        Error = "No driver is available right now",
                    });
                }

            }
            if (request.driver_id != null)
            {
                response = _rideManagerService.requestForRideEnd(request.driver_id);
                if (response.Type == "InvalidDriver")
                {
                    return Json(new
                    {
                        Success = false,
                        Error = "Invalid driver_id",
                    });
                }
                if (response.Type == "NotInRide")
                {
                    return Json(new
                    {
                        Success = false,
                        Error = "You are not  in a ride.",
                    });
                }
                try
                {
                    return Json(new { id = response.Ride.RiderId, driver_id = response.Ride.DriverId, rider_id = response.Ride.RiderId, status = response.Ride.Status });
                }
                catch(Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        Error = "Invalid Request.",
                    });
                }

                
            }
            return null;
            
        }

        [HttpGet(ApiRoutes.Rides.RideRequest)]
        public IActionResult DriverStatus(string driver_id = null, string rider_id =null)
        {
            var response = _rideManagerService.GetStatus(driver_id, rider_id);
            if(response.type== "InvalidDriver")
            {

                return Json(new
                {
                    Success = false,
                    Error = "Invalid driver_id",
                });
            }
            if(response.type == "InvalidRider")
            {
                return Json(new
                {
                    Success = false,
                    Error = "Invalid rider_id",
                });
            }
            if (response.type == "NotFound")
            {
                return Json(new
                {
                    Success = false,
                    Error = "User is not in a ride",
                });
            }
            return Json(new { id = response.RiderId, driver_id = response.DriverId, rider_id = response.RiderId, rider_phone = response.Phone, status = response.Status });
            
        }
    }
}
